package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.ComingSoon;

public class ComingSoonTest {

	@Test
	public void testGetId() {
		ComingSoon cs=new ComingSoon();
		cs.setId(2);
		assertTrue(cs.getId()==2);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGenre() {
		ComingSoon cs=new ComingSoon();
		cs.setGenre("Comedy");
		assertTrue(cs.getGenre()=="Comedy");
		//fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		ComingSoon cs=new ComingSoon();
		cs.setTitle("KGF2");
		assertTrue(cs.getTitle()=="KGF2");
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		ComingSoon cs=new ComingSoon();
		cs.setTitle("KGF2");
		assertTrue(cs.getTitle()=="KGF2");
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		ComingSoon cs=new ComingSoon();
		cs.setYear(2022);
		assertTrue(cs.getYear()==2022);
		//fail("Not yet implemented");
	}

}
