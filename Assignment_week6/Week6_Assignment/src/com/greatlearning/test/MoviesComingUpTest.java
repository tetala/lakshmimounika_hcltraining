package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.MoviesComingUp;

public class MoviesComingUpTest {

	@Test
	public void testGetId() {
		MoviesComingUp mu=new MoviesComingUp();
		mu.setId(1);
        assertTrue(mu.getId()==1);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetId() {
		MoviesComingUp mu=new MoviesComingUp();
		mu.setId(1);
        assertTrue(mu.getId()==1);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		MoviesComingUp mu=new MoviesComingUp();
		mu.setTitle("KGF");
        assertTrue(mu.getTitle()=="KGF");
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		MoviesComingUp mu=new MoviesComingUp();
		mu.setTitle("KGF");
        assertTrue(mu.getTitle()=="KGF");
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		MoviesComingUp mu=new MoviesComingUp();
		mu.setYear(2015);
        assertTrue(mu.getYear()==2015);
		//fail("Not yet implemented");
	}

}
