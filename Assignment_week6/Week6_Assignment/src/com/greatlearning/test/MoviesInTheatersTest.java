package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.MoviesInTheaters;

public class MoviesInTheatersTest {

	@Test
	public void testGetId() {
		MoviesInTheaters mit=new MoviesInTheaters();
		mit.setId(1);
		assertTrue(mit.getId()==1);
		//fail("Not yet implemented"); 
	}

	@Test
	public void testSetId() {
		MoviesInTheaters mit=new MoviesInTheaters();
		mit.setId(2);
	    assertTrue(mit.getId()==2);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetTitle() {
		MoviesInTheaters mit=new MoviesInTheaters();
		mit.setTitle("Baahubhali");
		assertTrue("Baahubhali", true);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		MoviesInTheaters mit=new MoviesInTheaters();
		mit.setTitle("RRR");
		assertTrue("RRR", true);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetRating() {
		MoviesInTheaters mit=new MoviesInTheaters();
		mit.setRating(5);
		assertTrue(mit.getRating()==5);
		//fail("Not yet implemented");
	}

}
