package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.TopRatedMovies;

public class TopRatedMoviesTest {

	@Test
	public void testGetId() {
		TopRatedMovies trm=new TopRatedMovies();
		trm.setId(2);
		assertTrue(trm.getId()==2);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetTitle() {
		TopRatedMovies trm=new TopRatedMovies();
		trm.setTitle("Indra");
		assertTrue(trm.getTitle()=="Indra");
		//fail("Not yet implemented");
	}

	@Test
	public void testGetYear() {
		TopRatedMovies trm=new TopRatedMovies();
		trm.setYear(2018);
		assertTrue(trm.getYear()==2018);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetYear() {
		TopRatedMovies trm=new TopRatedMovies();
		trm.setYear(2018);
		assertTrue(trm.getYear()==2018);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetRating() {
		TopRatedMovies trm=new TopRatedMovies();
		trm.setRating(1);
		assertTrue(trm.getRating()==1);
		//fail("Not yet implemented");
	}

}
