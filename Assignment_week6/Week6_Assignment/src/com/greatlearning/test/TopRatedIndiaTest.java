package com.greatlearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatlearning.bean.TopRatedIndia;

public class TopRatedIndiaTest {

	@Test
	public void testSetYear() {
		TopRatedIndia tri=new TopRatedIndia();
		tri.setYear(2018);
		assertTrue(tri.getYear()==2018);
		//fail("Not yet implemented");
	}

	@Test
	public void testGetGenre() {
		TopRatedIndia tri=new TopRatedIndia();
		tri.setGenre("Action");
		assertTrue(tri.getGenre()=="Action");
		//fail("Not yet implemented");
	}

	@Test
	public void testSetGmid() {
		TopRatedIndia tri=new TopRatedIndia();
		tri.setGenre("Fiction");
		assertTrue(tri.getGenre()=="Fiction");
		//fail("Not yet implemented");
	}

	@Test
	public void testGetRating() {
		TopRatedIndia tri=new TopRatedIndia();
		tri.setRating(5);
		assertTrue(tri.getRating()==5);
		//fail("Not yet implemented");
	}

	@Test
	public void testSetRating() {
		TopRatedIndia tri=new TopRatedIndia();
		tri.setRating(4);
		assertTrue(tri.getRating()==4);
		//fail("Not yet implemented");
	}

}
