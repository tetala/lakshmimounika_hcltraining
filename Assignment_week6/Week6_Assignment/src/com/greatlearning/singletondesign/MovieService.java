package com.greatlearning.singletondesign;

import java.util.Iterator;
import java.util.List;

import com.greatlearning.bean.ComingSoon;
import com.greatlearning.bean.MoviesComingUp;
import com.greatlearning.bean.MoviesInTheaters;
import com.greatlearning.bean.TopRatedIndia;
import com.greatlearning.bean.TopRatedMovies;

public class MovieService {
	public List<MoviesComingUp> findAllMovies(){
		MovieDao md=new MovieDao();
		List<MoviesComingUp>listofUpcoming=md.getAllUpcomingMovies();
		Iterator<MoviesComingUp>li=listofUpcoming.iterator();
		while(li.hasNext()) {
			MoviesComingUp mu=li.next();
		} 
		return listofUpcoming;
		
	}
	
	public List<MoviesInTheaters> findAllMoviesInTheaters(){
		MovieDao md=new MovieDao();
		List<MoviesInTheaters>listofMoviesNow=md.getAllMoviesInTheaters();
		Iterator<MoviesInTheaters>li=listofMoviesNow.iterator();
		while(li.hasNext()) {
			MoviesInTheaters mit=li.next();
		} 
		return listofMoviesNow;
		
	}
	
	public List<TopRatedIndia> findAllMoviesTopInIndia(){
		MovieDao md=new MovieDao();
		List<TopRatedIndia>listofTopMoviesIndia=md.getAllMoviesInTopIndia();
		Iterator<TopRatedIndia>li=listofTopMoviesIndia.iterator();
		while(li.hasNext()) {
			TopRatedIndia tri=li.next();
		} 
		return listofTopMoviesIndia;
		
	}
	public List<TopRatedMovies> findAllMoviesOnTop(){
		MovieDao md=new MovieDao();
		List<TopRatedMovies>listofTopMovies=md.getAllMoviesInTop();
		Iterator<TopRatedMovies>li=listofTopMovies.iterator();
		while(li.hasNext()) {
			TopRatedMovies trm=li.next();
		} 
		return listofTopMovies;
		
	}
	
	public List<ComingSoon> findAllMoviesComingSoon(){
		MovieDao md=new MovieDao();
		List<ComingSoon>listofMoviesComingSoon=md.getAllMoviesComingSoon();
		Iterator<ComingSoon>li=listofMoviesComingSoon.iterator();
		while(li.hasNext()) {
			ComingSoon cs=li.next();
		} 
		return listofMoviesComingSoon;
		
	}
}
