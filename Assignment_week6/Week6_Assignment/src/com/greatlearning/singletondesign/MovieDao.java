package com.greatlearning.singletondesign;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.ComingSoon;
import com.greatlearning.bean.MoviesComingUp;
import com.greatlearning.bean.MoviesInTheaters;
import com.greatlearning.bean.TopRatedIndia;
import com.greatlearning.bean.TopRatedMovies;

public class MovieDao {
	//Method for retriving All MoviesComingUp
	public List<MoviesComingUp> getAllUpcomingMovies(){
		List<MoviesComingUp>listOfUpComing=new ArrayList<>();
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement pst=con.prepareStatement("select * from moviescomingup");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				MoviesComingUp mcu=new MoviesComingUp();
				mcu.setId(rs.getInt(1));
				mcu.setTitle(rs.getString(2));
				mcu.setYear(rs.getInt(3));
				mcu.setGenre(rs.getString(4));
				listOfUpComing.add(mcu);				
			}
		} catch (Exception e) {
			System.out.println("InUpcomingUp"+e);
		}
		
		return listOfUpComing;
		
	}

	//Method for retriving MoviesInTheaters
	public List<MoviesInTheaters> getAllMoviesInTheaters(){
		List<MoviesInTheaters>listOfMoviesNow=new ArrayList<>();
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement pst=con.prepareStatement("select * from moviesintheaters");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				MoviesInTheaters mit=new MoviesInTheaters();
				mit.setId(rs.getInt(1));
				mit.setTitle(rs.getString(2));			
				mit.setYear(rs.getInt(3));
				mit.setRating(rs.getInt(4));
				mit.setGenre(rs.getString(5));
				listOfMoviesNow.add(mit);				
			}
		} catch (Exception e) {
			System.out.println("Intheaters"+e);
		}
		
		return listOfMoviesNow;
		
	}
	
	//Method for Retriving MoviesTopRatedInIndia
	public List<TopRatedIndia> getAllMoviesInTopIndia(){
		List<TopRatedIndia>listOfTopMoviesInIndia=new ArrayList<>();
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement pst=con.prepareStatement("select * from topratedindia");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				TopRatedIndia tri=new TopRatedIndia();
				tri.setId(rs.getInt(1));
				tri.setTitle(rs.getString(2));
				tri.setYear(rs.getInt(3));
				tri.setRating(rs.getInt(4));
				tri.setGenre(rs.getString(5));
				listOfTopMoviesInIndia.add(tri);				
			}
		} catch (Exception e) {
			System.out.println("Intopratedindia"+e);
		}
		
		return listOfTopMoviesInIndia;
		
	}
	
	//Method for Retriving TopRatedMovies
	public List<TopRatedMovies> getAllMoviesInTop(){
		List<TopRatedMovies>listOfTopMovies=new ArrayList<>();
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement pst=con.prepareStatement("select * from topratedmovies");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				TopRatedMovies trm=new TopRatedMovies();
				trm.setId(rs.getInt(1));
				trm.setTitle(rs.getString(2));
				trm.setYear(rs.getInt(3));
				trm.setRating(rs.getInt(5));
				trm.setGenre(rs.getString(4));
				
				listOfTopMovies.add(trm);				
			}
		} catch (Exception e) {
			System.out.println("InlistOfTopMovies"+e);
		}
		
		return listOfTopMovies;
		
	}
	//Method for Retriving ComingSoon Movies
	public List<ComingSoon> getAllMoviesComingSoon(){
		List<ComingSoon>listOfComingSoon=new ArrayList<>();
		try {
			Connection con=DbResource.getDbConnection();
			PreparedStatement pst=con.prepareStatement("select * from comingsoon");
			ResultSet rs=pst.executeQuery();
			while(rs.next()) {
				ComingSoon cs=new ComingSoon();
				cs.setId(rs.getInt(1));
				cs.setTitle(rs.getString(2));
				cs.setYear(rs.getInt(3));
				cs.setGenre(rs.getString(4));
				
				listOfComingSoon.add(cs);				
			}
		} catch (Exception e) {
			System.out.println("InlistOfcomingsoon"+e);
		}
		
		return listOfComingSoon;
		
	}
}
