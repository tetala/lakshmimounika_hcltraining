package com.greatlearning.bean;

public class FactoryDesignMethod {
	public static Movies getInstancetype(String type) {
		if(type.equals("moviesComingup")) {
			return new MoviesComingUp();
		}else if(type.equals("MoviesInTheaters")) {
			return new MoviesInTheaters();			
		}else if(type.equals("TopRatedIndia")) {
			return new TopRatedIndia();
		}else if(type.equals("TopRatedMovies")) {
			return new TopRatedMovies();
		}else if(type.equals("ComingSoon")) {
			return new ComingSoon();
		}
		else {
		return null;
		}
	}

}
