package com.greatlearning.bean;

public class ComingSoon implements Movies {
	private int id;
	private String title;
	private int year;
	private String genre;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	@Override
	public void call() {
		// TODO Auto-generated method stub
		
	}
	@Override
	public String toString() {
		return "ComingSoon [id=" + id + ", title=" + title + ", year=" + year + ", genre=" + genre + "]";
	}
	
	

}
