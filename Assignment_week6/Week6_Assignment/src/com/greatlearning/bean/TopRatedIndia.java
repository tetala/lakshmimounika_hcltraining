package com.greatlearning.bean;


public class TopRatedIndia implements Movies {
	private int id;
	private String title;
	private int year;
	private int rating;
	private String genre;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	@Override
	public void call() {
		System.out.println("listofTopRatedMovies");
		
	}
	@Override
	public String toString() {
		return "TopRatedIndia [id=" + id + ", title=" + title + ", year=" + year + ", rating=" + rating + ", genre="
				+ genre + "]";
	}
	
	
}
