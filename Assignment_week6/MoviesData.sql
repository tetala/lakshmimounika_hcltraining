create database Movies_LakshmiMounika_Hcl;
 
use Movies_LakshmiMounika_Hcl;

create table genretable(gid int,genremain varchar(20) primary key);
insert into genretable values(1,"Action");
insert into genretable values(2,"Fiction");
insert into genretable values(3,"Thriller");
insert into genretable values(4,"Comedy");
insert into genretable values(5,"Romantic");
select * from genretable;


create table moviescomingup(id int primary key,title varchar(20),year int,genre varchar(20) ,foreign key(genre) references genretable(genremain));
insert into moviescomingup values(1,"Pushpa",2023,"Action");
insert into moviescomingup values(2,"RRR",2022,"Comedy");
insert into moviescomingup values(3,"ManInHills",2023,"Romantic");
insert into moviescomingup values(4,"Smarty",2022,"Thriller");
insert into moviescomingup values(5,"Shimlaflaw",2021,"Action");
select * from moviescomingup;

create table moviesintheaters(id int primary key,title varchar(20),year int,rating int,genre varchar(20),foreign key(genre) references genretable(genremain));
insert into moviesintheaters values(1,"Gang",2021,3,"Fiction");
insert into moviesintheaters values(2,"Hassel",2021,5,"Comedy");
insert into moviesintheaters values(3,"Shakthi",2021,3,"Comedy");
insert into moviesintheaters values(4,"Singam",2021,5,"Romantic");
insert into moviesintheaters values(5,"Rawbyte",2021,5,"Thriller");
select * from moviesintheaters;

create table topratedindia(id int primary key,title varchar(20),year int,rating int,genre varchar(20),foreign key(genre) references genretable(genremain));
insert into topratedindia values(1,"Baahubhali",2018,3,"Fiction");
insert into topratedindia values(2,"Karna",2019,5,"Thriller");
insert into topratedindia values(3,"KGF",2019,3,"Comedy");
insert into topratedindia values(4,"JaiBheem",2021,5,"Action");
insert into topratedindia values(5,"Jersey",2018,5,"Fiction");
select * from topratedindia;

create table topratedmovies(id int primary key,title varchar(20),year int,rating int,genre varchar(20),foreign key(genre) references genretable(genremain));
insert into topratedmovies values(1,"Indra",2010,4,"Action");
insert into topratedmovies values(2,"Dangal",2016,4,"Thriller");
insert into topratedmovies values(3,"Krish",2018,4,"Comedy");
insert into topratedmovies values(4,"Dabang",2016,5,"Action");
insert into topratedmovies values(5,"ChennaiExpress",2017,5,"Thriller");
select * from topratedmovies;

create table comingsoon(id int primary key,title varchar(20),year int,genre varchar(20),foreign key(genre) references genretable(genremain));
insert into comingsoon values(1,"Adhipurush",2024,"Fiction");
insert into comingsoon values(2,"Kgf2",2025,"Action");
insert into comingsoon values(3,"Radhesyam",2023,"Thriller");
insert into comingsoon values(4,"Laalisingh",2022,"Comedy");
insert into comingsoon values(5,"Avatar2",2024,"Fiction");
select * from comingsoon;




